public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        //1. Una clase abstracta no puede ser instanciada. TRUE
        //BaseView baseView = new BaseView(); Error

        //2. Una clase puede implementar más de una interface TRUE
        Box box = new Box();
        int area = box.area();
        System.out.println("area "+area);
        box.print();

        //3. Una clase puede extender mas de una clase FALSE
        //public class Bicycle extends Vehicle,Energy
        Vehicle bicycle = new Bicycle();
        System.out.println("bicycle "+bicycle);

        //4. Una interface puede extender otra interface TRUE
        //public interface Device extends Printer

        /*
        Asumiendo que existen las siguientes interfaces y clases java:
            interface Bounceable { }​​​​​
            interface Serializable { }​​​​​
            interface Moveable { }​​​​​
            interface Spherical{ }​​
            class Foo { } ​
            interface Baz { } ​
            Indique si es correcto/incorrecto en cada caso:
        */

        //5. abstract class Ball implements Bounceable { }​​ TRUE
        //public abstract class Ball implements Bounceable

        //6. public class Ball implements Bounceable, Serializable{ ... } TRUE
        //public class Ball implements Bounceable, Serializable

        //7. public interface Bounceable extends Moveable { }  TRUE
        //public interface Bounceable extends Moveable

        //8. interface Bounceable extends Moveable, Spherical { TRUE
        //public interface Bounceable extends Moveable, Spherical

        //9. class Bar implements Foo { } FALSE
        // Foo es una clase

        //10. Interface Fee implements Baz { } FALSE
        //public interface Fee implements Baz
        //Solo se puede hacer extends entre interfaces

        //11. Interface Zee implements Foo { } FALSE
        // Foo es una clase, implements es para interfaces

        //12. Interface Zoo extends Foo { } FALSE
        //public interface Zoo extends Foo
        //Solo se puede hacer extends entre interfaces

        //13. class Toon extends Foo, Ball { } FALSE
        //Solo se puede extender de una clase, no de una interface
        //public class Toon extends Foo,Ball
    }
}
